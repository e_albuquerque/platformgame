﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ConstantScaleHeightInEditor : MonoBehaviour {

	public float worldHeight;

	// Use this for initialization
	void Start () {
		if (Application.isPlaying)
			return;
		
		worldHeight = this.transform.lossyScale.y;
	}
	
	// Update is called once per frame
	void Update () {
		if (Application.isPlaying)
			return;

		if (this.transform.lossyScale.y != worldHeight)
			UpdateScale ();
	}

	void UpdateScale()
	{
		float lossyScaleY = this.transform.lossyScale.y;
		this.transform.localScale = new Vector3(this.transform.localScale.x, worldHeight / lossyScaleY  * this.transform.localScale.x, this.transform.localScale.z);				
	}
}
