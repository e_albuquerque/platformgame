﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public float speed = 1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.parent.Translate (Vector3.right * Time.deltaTime * speed);	
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		print ("entrou0");
		NewPlayer2 player = c.gameObject.GetComponent<NewPlayer2> ();
		if ( player != null) 
		{
			print ("entrou1");
			if (player.rigidBody2d.velocity.y < 0) {
				print ("entrou2");
				float recoilJumpSpeed = 0f;
				if (Input.GetButton ("Fire1"))
					recoilJumpSpeed = player.jumpSpeed;
				else
					recoilJumpSpeed = player.jumpSpeed / 2f;

				player.rigidBody2d.velocity = new Vector2 (player.rigidBody2d.velocity.x, recoilJumpSpeed);
				this.gameObject.SetActive (false);
			}
		}
			
	}
}
