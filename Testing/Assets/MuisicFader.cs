﻿using UnityEngine;
using System.Collections;

public class MuisicFader : MonoBehaviour {

	public AudioClip music;
	AudioSource audioSource;
	public float maximumVolume = 1f;
	public float fadeSpeed = 1f;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		audioSource.clip = music;
		audioSource.volume = 0f;
		DontDestroyOnLoad (this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FadeMusic(float targetVolume)
	{
		audioSource.volume = Mathf.Lerp (audioSource.volume, targetVolume, Time.deltaTime * fadeSpeed);
	}

	IEnumerator MuisicFadeIn()
	{
		audioSource.Play ();

		do
		{
			FadeMusic(maximumVolume);

			if (audioSource.volume >= 0.95f * maximumVolume)
			{
				yield break;
			}
			else
			{
				yield return null;
			}
		} while (true);				
	}

	void OnTriggerEnter2D (Collider2D c) 
	{
		StartCoroutine (MuisicFadeIn ());
	}
}
