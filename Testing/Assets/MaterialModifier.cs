﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshRenderer))]
public class MaterialModifier : MonoBehaviour {

	public bool setupTrigger = false;

	public Material sourceMaterial;
	public float scaleMultipier = 1f;

	float currentScaleMultiplier;
	Material material;
	Vector3 currentScale = Vector3.one;

	string currentGameObjectName; //Para evitar que um compenente de um GO altere o material de outro GO TODO: Melhorar

	// Use this for initialization
	void Start () {
		if (Application.isPlaying)
			return;
		
		setupTrigger = false;	

		if (currentGameObjectName != null)
			currentGameObjectName = this.gameObject.name;

		if (currentGameObjectName != this.gameObject.name) 
		{ 
			currentGameObjectName = this.gameObject.name;
			Setup ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Application.isPlaying)
			return;

		if (setupTrigger == true) 
		{
			setupTrigger = false;
			Setup ();
		}

		if (currentScale != this.transform.lossyScale || currentScaleMultiplier != scaleMultipier)
			UpdateMaterial ();
	}

	void Setup()
	{
		if (sourceMaterial == null) 
		{
			Debug.Log ("No source material. Returning...");
			return;
		}

		Debug.Log ("Setting Up new Material...");

		material = new Material (sourceMaterial);

		MeshRenderer mr = GetComponent<MeshRenderer> ();
		mr.material = material;

		material.mainTextureOffset = new Vector2(Random.Range(0f,1f),Random.Range(0f,1f));
		material.mainTextureScale = new Vector2 (this.transform.lossyScale.x, this.transform.lossyScale.y) * scaleMultipier;

		currentScale = this.transform.lossyScale;

		currentScaleMultiplier = scaleMultipier;

		Debug.Log ("New material set up.");
	}

	void UpdateMaterial()
	{
		if (material == null)
			return;

		material.mainTextureScale = new Vector2 (this.transform.lossyScale.x, this.transform.lossyScale.y) * scaleMultipier;

		currentScale = this.transform.lossyScale; 

		currentScaleMultiplier = scaleMultipier;
	}
}
