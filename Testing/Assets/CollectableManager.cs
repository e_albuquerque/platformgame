﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectableManager : MonoBehaviour {

	List<CollectableScript> collectables = new List<CollectableScript>(); 

	int nCollected = 0;

	bool allCollected = false;

	public Transform rewardPostion;
	public GameObject reward;
	public TextMesh text;

	void Start()
	{
		nCollected = 0;
		allCollected = false;
	}

	void Update()
	{
		if (collectables.Count == nCollected && allCollected == false) 
		{
			allCollected = true;
			FindObjectOfType<ScreenFader> ().FlashWhite (1f);
			//FindObjectOfType<NewPlayer2> ().GetComponent<Rigidbody2D> ().MovePosition(new Vector2(rewardPostion.position.x,rewardPostion.position.y));
			reward.SetActive (true);
			text.text = "You look very elegant,\n my friend";

		}
		
	}

	public void AddCollectable(CollectableScript c)
	{
		collectables.Add (c);
	}

	public int GetTotalCollectables()
	{
		return collectables.Count;
	}

	public void GotACollectable()
	{
		nCollected++;
	}

	public int GetNumberCollected()
	{
		return nCollected;
	}

	public bool AllCollected()
	{
		if (nCollected >= collectables.Count)
			return true;
		else
			return false;
	}
}
