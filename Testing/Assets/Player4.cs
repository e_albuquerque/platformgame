﻿using UnityEngine;
using System.Collections;

public class Player4 : MonoBehaviour {

	Rigidbody2D rigidBody2d;

	public float maxSpeedX, maxSpeedY;
	public float maxSpeedXOnGround;

	public float accel, groundAccel, airAccel;

	public bool cLeft;
	public bool cRight;
	public bool onGround;

	public LayerMask whatIsSolid;
	public float solidCheckRadius = 0.01f;

	public Transform groundCheck,leftCheck,rightCheck;
	Collider2D groundCheckCol,leftCheckCol,rightCheckCol;

	float axisHorizontal, axisVertical;

	// Use this for initialization
	void Start () {
		
		onGround = false;
		cLeft = false;
		cRight = false;
		
		rigidBody2d = this.gameObject.GetComponent<Rigidbody2D>();
		
		groundCheckCol = groundCheck.GetComponent<BoxCollider2D>();
		leftCheckCol = leftCheck.GetComponent<BoxCollider2D>();
		rightCheckCol = rightCheck.GetComponent<BoxCollider2D>();	
	}
	
	// Update is called once per frame
	void Update () {

		axisHorizontal = Input.GetAxis ("Horizontal");
		axisVertical = Input.GetAxis ("Vertical");

		onGround = Physics2D.IsTouchingLayers( groundCheckCol,whatIsSolid);
		cLeft = Physics2D.IsTouchingLayers( leftCheckCol,whatIsSolid);
		cRight = Physics2D.IsTouchingLayers( rightCheckCol,whatIsSolid);

		if (onGround)
		{
			accel = groundAccel;
		}
		else
		{
			accel = airAccel;
		}

		rigidBody2d.AddForce (new Vector2 (axisHorizontal * accel, 0.0f));
	
	}

	public float AccelerateUniformiy (float currentValue, float changeRatio, float maxValue = Mathf.Infinity)
	{
		maxValue = Mathf.Abs (maxValue);

		currentValue += changeRatio;
		if ( currentValue > maxValue)
			currentValue = maxValue;
		else if ( currentValue < - maxValue)
			currentValue = - maxValue;
		
		return currentValue;
	}
}
