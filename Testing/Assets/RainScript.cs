﻿using UnityEngine;
using System.Collections;

public class RainScript : MonoBehaviour {

	GameObject playerGO;
	AudioSource audioSource;
	float startVolume;
	public float insideCavernMultiplier;
	public float minDistance;
	public float maxDistance;
	public float volumeChangeTime = 2f;
	float volumeChangeRate = 0f;
	public float fadeVolumeTime = 10f;
	float volumeFadeRate = 0f;
	public float fadeRainTime = 10f;
	float fadeRainRate = 0f;

	bool turningOff = false;

	public Vector2 cavernEntrance = Vector2.zero;

	ParticleSystem particles;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();	
		startVolume = audioSource.volume;
		audioSource.volume = 0f;

		playerGO = GameObject.FindGameObjectWithTag("Player");

		particles = transform.parent.GetComponent<ParticleSystem> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (turningOff)
			return;

		if (playerGO.transform.position.y > cavernEntrance.y) {
			SetRainVolume(startVolume);

		} else {

			float distanceX = Mathf.Abs (playerGO.transform.position.x - cavernEntrance.x);
            //Debug.Log(distanceX);
			if (distanceX < minDistance) {
				SetRainVolume (startVolume * insideCavernMultiplier);
			} else if (distanceX >= minDistance && distanceX < maxDistance) {
				float newVolume = startVolume * insideCavernMultiplier * (1f - ((distanceX - minDistance) / (maxDistance - minDistance)));
				SetRainVolume (newVolume);
			} else {
				SetRainVolume (0f);
			}
		}	
	}

	void SetRainVolume(float newVolume)
	{
		audioSource.volume = Mathf.SmoothDamp (audioSource.volume, newVolume, ref volumeChangeRate, volumeChangeTime);
	}



	void OnDrawGizmos()
	{
		Gizmos.DrawCube (new Vector3( cavernEntrance.x, cavernEntrance.y,0f),Vector3.one);
	}


	void OnTriggerEnter2D (Collider2D c) 
	{
		turningOff = true;
		StartCoroutine (StopRain ());
	}

	public IEnumerator StopRain()
	{
		float timeCounter = 0f;
		float initialVolume = audioSource.volume;
		int initialMaxParticles = particles.maxParticles;

		do
		{
			volumeFade(initialVolume, 0f,timeCounter );
			rainFade(initialMaxParticles, 0,timeCounter);

			timeCounter +=Time.deltaTime;

			if (particles.maxParticles <= 2f && audioSource.volume < 0.01f)
			{
				this.transform.parent.gameObject.SetActive(false);
				yield break;
			}
			else
			{
				yield return null;
			}
		} while (true);				
	}

	void volumeFade(float initialValue, float newVolume, float t)
	{
		audioSource.volume = Mathf.Lerp (initialValue, newVolume, t / fadeVolumeTime);
	}

	void rainFade(int initialValue, int newMaxParticles, float t)
	{
		particles.maxParticles = (int) Mathf.Lerp ((float) initialValue, (float) newMaxParticles, t / fadeRainTime);
	}
}
