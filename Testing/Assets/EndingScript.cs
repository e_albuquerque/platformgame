﻿using UnityEngine;
using System.Collections;

public class EndingScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
			
	}


	void OnTriggerEnter2D (Collider2D c) 
	{
		StartCoroutine (EndRoutine ());
	}

	public IEnumerator EndRoutine()
	{
		NewPlayer2 player = FindObjectOfType<NewPlayer2> ();
		//player.GetComponent<Rigidbody2D> ().velocity = new Vector2( player.GetComponent<Rigidbody2D> ().velocity.x * 0.7f, player.GetComponent<Rigidbody2D> ().velocity.y);
		player.maxSpeedX = 0;
		player.maxSpeedY = 0;

        FindObjectOfType<Timer>().StopTimer();

		yield return new WaitForSeconds(5f);

		FindObjectOfType<ScreenFader> ().EndScene ("Ending");
	}
}
