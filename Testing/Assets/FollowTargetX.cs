﻿using UnityEngine;
using System.Collections;

public class FollowTargetX : MonoBehaviour {

	public Transform targetTransform;

	// Use this for initialization
	void Start () {
		transform.position = new Vector3 (targetTransform.position.x, transform.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (targetTransform.position.x, transform.position.y, transform.position.z);	
	}
}
