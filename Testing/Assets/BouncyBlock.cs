﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncyBlock : MonoBehaviour {

    public float _bounceSpeed = 1f;

	// Use this for initialization
	void Start () {


		
	}
	
	// Update is called once per frame
	void Update () {


		
	}


    private void OnCollisionEnter2D(Collision2D collision)
    {
        NewPlayer2 player = collision.gameObject.GetComponent<NewPlayer2>();

        if (player != null)
        {
            Debug.Log("entrou");
            player.rigidBody2d.velocity = new Vector2(_bounceSpeed * - Mathf.Sign(player.rigidBody2d.velocity.x), player.rigidBody2d.velocity.y);
        }

    }
}
