﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cheating : MonoBehaviour {

    public Transform _teleportTransform;
    NewRope _rope;
    NewPlayer2 _player;

	// Use this for initialization
	void Start () {
        _player = FindObjectOfType<NewPlayer2>();
        _rope = FindObjectOfType<NewRope>();

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            _player.transform.position = _teleportTransform.position;
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            _rope.enabled = true;
        }

    }
}
