﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndingScreen : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeSinceLevelLoad > 5f) {
			if (Input.anyKeyDown) {
				MuisicFader m = FindObjectOfType<MuisicFader> ();
				if (m != null) {
					Destroy (m.gameObject);
				}
				SceneManager.LoadScene (0);
					
			}
		}
	
	}
}
