﻿using UnityEngine;
using System.Collections;

public class CollectableScript : MonoBehaviour {

	public AudioClip gotItemSound;
	CollectableManager cm;
	TextMesh text;
	MeshRenderer textRenderer;

	float textShownTime = 0f;

	// Use this for initialization
	void Start () {
		cm = transform.parent.GetComponent<CollectableManager> ();
		cm.AddCollectable (this);
		textShownTime = 0f;

		text = GetComponentInChildren<TextMesh> ();
		textRenderer = text.gameObject.GetComponent<MeshRenderer> ();
	}

	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D c) {

        if (c.gameObject.tag == "Player")
        {

            //Debug.Log("entrou");

            this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;


            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }

            GetComponent<AudioSource>().PlayOneShot(gotItemSound);

            cm.GotACollectable();

            StartCoroutine(ShowText(4));
        }
	}

	IEnumerator ShowText(float timeDisplay)
	{
		text.gameObject.SetActive (true);
		textRenderer.enabled = true;

		text.text = cm.GetNumberCollected ().ToString () + " / " + cm.GetTotalCollectables ().ToString ();

		yield return new WaitForSeconds (timeDisplay);

		textRenderer.enabled = false;
		text.gameObject.SetActive (false);

	}


}
