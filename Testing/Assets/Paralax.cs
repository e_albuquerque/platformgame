﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paralax : MonoBehaviour {

    Camera _camera;
    Vector3 _lastCameraPosition;

    public float _gain = 0f;

	// Use this for initialization
	void Start () {
        _camera = Camera.main;
        _lastCameraPosition = _camera.transform.position;


    }
	

	void LateUpdate () {
        Vector3 cameraPosition = _camera.transform.position;

        Vector3 delta = cameraPosition - _lastCameraPosition;

        this.transform.position = this.transform.position + _gain * delta;

        _lastCameraPosition = cameraPosition;
	}
}
