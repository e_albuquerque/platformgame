﻿using UnityEngine;
using System.Collections;

public class CameraCue : MonoBehaviour {

	CameraController cameraController;

	public float targetCameraSize = 19f;

	// Use this for initialization
	void Start () {

		cameraController = FindObjectOfType<CameraController> ();	
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		cameraController.currentTargetSize = targetCameraSize;
	}

	void OnTriggerExit2D(Collider2D c)
	{
		cameraController.currentTargetSize = cameraController.startSize;
	}

}
