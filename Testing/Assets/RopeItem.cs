﻿using UnityEngine;
using System.Collections;

public class RopeItem : MonoBehaviour {

	NewRope newRope;
	public AudioClip gotItemSound;

	public TextMesh text1, text2;

	// Use this for initialization
	void Start () {
		newRope = FindObjectOfType<NewRope> ();

		text1.gameObject.SetActive (false);
		text2.gameObject.SetActive (false);
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D c) {
		newRope.enabled = true;
		this.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
		this.gameObject.GetComponent<MeshRenderer> ().enabled = false;

		for (int i = 0; i < transform.childCount; i++) 
		{
			transform.GetChild (i).gameObject.SetActive (false);
		}

		GetComponent<AudioSource> ().PlayOneShot (gotItemSound);


		StartCoroutine (FreezeAndShowText (7f));
	}


	IEnumerator FreezeAndShowText(float time)
	{
		float startTime = Time.realtimeSinceStartup;

		Time.timeScale = 0.0f;

		while (Time.realtimeSinceStartup < startTime + time) 
		{
			yield return 0;
		}			

		Time.timeScale = 1f;

		text1.gameObject.SetActive (true);
		text2.gameObject.SetActive (true);
	}
}
