﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    [SerializeField]
    Text _text;
    float _timer;
    bool _showTimer;
    bool _blinkShow;
    float _blinkTimer = 0f;
    [SerializeField]
    float _blinkPeriod = 1f;

    bool _stopped = false;

	// Use this for initialization
	void Start () {
        _text = GetComponent<Text>();
        _timer = 0f;
        _showTimer = false;
        _blinkShow = true;
        _stopped = false;
        _blinkTimer = 0f;
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButtonDown("TimerButton"))
        {
            _showTimer = !_showTimer;
            ShowTimer(true);
        }

        _timer = Time.timeSinceLevelLoad;

        if (!_stopped)
        {
            string minutes = Mathf.Floor(_timer / 60).ToString("00");
            string seconds = Mathf.Floor(_timer % 60).ToString("00");
            string centiseconds = ((_timer % 1) * 100f).ToString("00");

            _text.text = minutes + " : " + seconds + " : " + centiseconds;
        }
        else
        {
            if (_blinkTimer > _blinkPeriod)
            {
                _blinkTimer = 0f;
                Blink();
            }
            else
                _blinkTimer += Time.deltaTime;
        }
    }

    public void StopTimer()
    {
        _stopped = true;
    }

    void ShowTimer(bool show)
    {
        _text.enabled = show && _showTimer;
    }

    void Blink()
    {
        _blinkShow = !_blinkShow; 
        ShowTimer(_blinkShow);
    }
}
