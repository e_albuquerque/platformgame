﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScreenFader : MonoBehaviour
{
	public RawImage FadeImg;
	public float fadeSpeed = 1.5f;
	public bool sceneStarting = true;


	void Awake()
	{
		//FadeImg.rectTransform.localScale = new Vector2(Screen.width, Screen.height);

    }

	void Update()
	{
        // If the scene is starting...
        if (sceneStarting)
        {
            // ... call the StartScene function.
            StartScene();
            //FadeImg.CrossFadeColor(Color.clear,5f,true,true);

        }
	}


    void FadeToClear()
	{
        // Lerp the colour of the image between itself and transparent.
        FadeImg.color = Color.Lerp(FadeImg.color, Color.clear, fadeSpeed * Time.deltaTime);
        //FadeImg.color = new Color(0.5, 0.5f, 0.5f, 0.5f);

    }


    void FadeToBlack()
	{
		// Lerp the colour of the image between itself and black.
		FadeImg.color = Color.Lerp(FadeImg.color, Color.black, fadeSpeed * Time.deltaTime);
	}

	void FadeToWhite()
	{
		FadeImg.color = Color.Lerp(FadeImg.color, Color.white, fadeSpeed * Time.deltaTime);
	}


	void StartScene()
	{
		// Fade the texture to clear.
		FadeToClear();

		// If the texture is almost clear...
		if (FadeImg.color.a <= 0.05f)
		{
			// ... set the colour to clear and disable the RawImage.
			FadeImg.color = Color.clear;
			FadeImg.enabled = false;

			// The scene is no longer starting.
			sceneStarting = false;
		}
	}

	public void FlashWhite(float time)
	{
		StartCoroutine (FlashWhiteRoutine(time));
	}

	IEnumerator FlashWhiteRoutine(float time)
	{
		// Make sure the RawImage is enabled.
		FadeImg.enabled = true;
		FadeImg.color = Color.white;

		FindObjectOfType<NewPlayer2> ().gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;
		yield return new WaitForSeconds (time);

		do
		{
			FadeToClear();

			if (FadeImg.color.a <= 0.05f)
			{
				FindObjectOfType<NewPlayer2> ().gameObject.GetComponent<Rigidbody2D> ().isKinematic = false;
				FadeImg.enabled = false;
				yield break;
			}
			else
			{
				yield return null;
			}
		} while (true);				
	}


	public IEnumerator EndSceneRoutine(string sceneName)
	{
		// Make sure the RawImage is enabled.
		FadeImg.enabled = true;
		do
		{
			// Start fading towards black.
			FadeToBlack();

			// If the screen is almost black...
			if (FadeImg.color.a >= 0.98f)
			{
				// ... reload the level
				FadeImg.color = Color.black;
				yield return new WaitForSeconds(2f);
				SceneManager.LoadScene(sceneName);
				yield break;
			}
			else
			{
				yield return null;
			}
		} while (true);
	}

	public void EndScene(string sceneName)
	{
		sceneStarting = false;
		StartCoroutine("EndSceneRoutine", sceneName);
	}
}