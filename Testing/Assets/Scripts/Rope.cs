﻿using UnityEngine;
using System.Collections;

public class Rope : MonoBehaviour {

	public GameObject objectAttached;
	public Vector3 localAnchorPoint;
	public Vector2 anchorPoint;
	public float radius;
	public float maxDistance = 100f;
    public float maxSearchAngle;
    public float angleDiscretization;

	Vector2 aimingDirection; 
	public bool ropeIsActive = false;

	LineRenderer lineRenderer;

    bool showAimingDirection;

	// Use this for initialization
	void Start () {

		lineRenderer = gameObject.GetComponent<LineRenderer> ();
		if (lineRenderer == null) {
			lineRenderer = gameObject.AddComponent<LineRenderer>();
		}
			
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown("Fire2"))
		{
			if (ropeIsActive == false)
			{
				aimingDirection = new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));
                RaycastHit2D hit = new RaycastHit2D();
                Vector2 searchDirection = aimingDirection;
                for (float searchAngle = 0.0f; searchAngle < maxSearchAngle; searchAngle += angleDiscretization)
                {
                    searchDirection = new Vector2(aimingDirection.x * Mathf.Cos(Mathf.Deg2Rad * searchAngle) - aimingDirection.y * Mathf.Sin(Mathf.Deg2Rad * searchAngle),
                        aimingDirection.x * Mathf.Sin(Mathf.Deg2Rad * searchAngle) + aimingDirection.y * Mathf.Cos(Mathf.Deg2Rad * searchAngle));
                    
                    hit = Physics2D.Raycast(this.GetComponent<Rigidbody2D>().position, searchDirection, maxDistance);
                    if (hit.collider != null)
                        break;

                    searchDirection = new Vector2(aimingDirection.x * Mathf.Cos(Mathf.Deg2Rad * -searchAngle) - aimingDirection.y * Mathf.Sin(Mathf.Deg2Rad * -searchAngle),
                        aimingDirection.x * Mathf.Sin(Mathf.Deg2Rad * -searchAngle) + aimingDirection.y * Mathf.Cos(Mathf.Deg2Rad * -searchAngle));

                    hit = Physics2D.Raycast(this.GetComponent<Rigidbody2D>().position, searchDirection, maxDistance);
                    if (hit.collider != null)
                        break;
                }

				if (hit.collider != null)
				{
					objectAttached = hit.collider.gameObject;
					localAnchorPoint = objectAttached.transform.InverseTransformPoint (new Vector3(hit.point.x,hit.point.y,0.0f));
					anchorPoint = new Vector2(objectAttached.transform.TransformPoint(localAnchorPoint).x,objectAttached.transform.TransformPoint(localAnchorPoint).y);
					ropeIsActive = true;
					lineRenderer.enabled = true;
				}
			}
		}
		
		if (Input.GetButtonUp("Fire2"))
		{
			if (ropeIsActive == true)
			{
				ropeIsActive = false;
				lineRenderer.enabled = false;
			}
		}

		if (ropeIsActive == true)
		{

			anchorPoint = new Vector2(objectAttached.transform.TransformPoint(localAnchorPoint).x,objectAttached.transform.TransformPoint(localAnchorPoint).y);
			lineRenderer.SetPosition(0,anchorPoint);
			lineRenderer.SetPosition(1,this.gameObject.transform.position);
		}
	
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 gizmoPos3D = gameObject.transform.position + new Vector3(aimingDirection.x, aimingDirection.y, 0.0f).normalized * maxDistance;
        Gizmos.DrawSphere(gizmoPos3D , 0.5f);
    }
}
