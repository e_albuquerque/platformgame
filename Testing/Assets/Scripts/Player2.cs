﻿using UnityEngine;
using System.Collections;

public class Player2 : MonoBehaviour
{

    public float m = 1.0f;

    public float groundAccel = 1.3f;
    public float groundFric = 2.47f;
    public float airAccel = 0.975f;
    public float airFric = 0.13f;
    public float vxMax = 8.45f;
    public float vyMax = 22.2f;
    public float jumpHeight = 15.6f;
    public float gravNorm = 0.65f;
    public float gravSlide = 0.325f;
    public int clingFrames = 8;

    public bool cLeft;
    public bool cRight;
    public bool onGround;

    bool canStick;
    bool sticking;
    int clingAlarm = 0;

    public LayerMask whatIsSolid;
    public float solidCheckRadius = 0.01f;

    public Transform groundCheck, leftCheck, rightCheck;
    Collider2D groundCheckCol, leftCheckCol, rightCheckCol;

    bool kLeft, kRight, kUp, kDown, kJumpDown, kJumpRelease;

    Rigidbody2D rigidBody2d;

    float axisHorizontal, axisVertical;

    Rope rope;
    public bool usingRope;


    // Use this for initialization
    void Start()
    {

        groundAccel *= m;
        groundFric *= m;
        airAccel *= m;
        airFric *= m;
        vxMax *= m;
        vyMax *= m;
        jumpHeight *= m;
        gravNorm *= m;
        gravSlide *= m;

        canStick = true;
        sticking = false;

        onGround = false;
        cLeft = false;
        cRight = false;

        clingAlarm = 0;

        rigidBody2d = this.gameObject.GetComponent<Rigidbody2D>();

        groundCheckCol = groundCheck.GetComponent<BoxCollider2D>();
        leftCheckCol = leftCheck.GetComponent<BoxCollider2D>();
        rightCheckCol = rightCheck.GetComponent<BoxCollider2D>();

        rope = gameObject.GetComponent<Rope>();
        if (rope == null)
        {
            rope = gameObject.AddComponent<Rope>();
        }
        usingRope = false;
    }

    // Update is called once per frame
    void Update()
    {

        axisHorizontal = Input.GetAxis("Horizontal");
        axisVertical = Input.GetAxis("Vertical");

        kLeft = (axisHorizontal < 0f) ? true : false;
        kRight = (axisHorizontal > 0f) ? true : false;
        kUp = (axisVertical > 0f) ? true : false;
        kDown = (axisVertical < 0f) ? true : false;

        kJumpDown = Input.GetButtonDown("Fire1");
        kJumpRelease = Input.GetButtonUp("Fire1");


        float tempAccel, tempFric;
        float vx, vy;

        vx = rigidBody2d.velocity.x;
        vy = rigidBody2d.velocity.y;

        //		onGround = Physics2D.OverlapCircle (groundCheck.position, solidCheckRadius, whatIsSolid);
        //		cLeft = Physics2D.OverlapCircle (leftCheck.position, solidCheckRadius, whatIsSolid);
        //		cRight = Physics2D.OverlapCircle (rightCheck.position, solidCheckRadius, whatIsSolid);

        onGround = Physics2D.IsTouchingLayers(groundCheckCol, whatIsSolid);
        cLeft = Physics2D.IsTouchingLayers(leftCheckCol, whatIsSolid);
        cRight = Physics2D.IsTouchingLayers(rightCheckCol, whatIsSolid);

        // Apply the correct form of acceleration and friction
        if (onGround)
        {
            tempAccel = groundAccel * Mathf.Abs(axisHorizontal);
            tempFric = groundFric;
        }
        else
        {
            tempAccel = airAccel * Mathf.Abs(axisHorizontal);
            tempFric = airFric;
        }

        // Reset wall cling
        if ((!cRight && !cLeft) || onGround)
        {
            canStick = true;
            sticking = false;
        }

        // Cling to wall
        if (((kRight && cLeft) || (kLeft && cRight)) && canStick && !onGround)
        {
            clingAlarm = clingFrames;
            sticking = true;
            canStick = false;
        }

        // Check clingAlarm
        if (sticking == true && clingAlarm <= 0)
        {
            sticking = false;
        }

        // Handle gravity
        if (!onGround)
        {
            if ((cLeft || cRight) && vy <= 0)
            {
                // Wall slide
                vy = Common.Approach(vy, -vyMax, gravSlide);
            }
            else
            {
                // Fall normally
                vy = Common.Approach(vy, -vyMax, gravNorm);
            }
        }

        // Left 
        if (kLeft && !kRight && !sticking)
        {
            // Apply acceleration left
            vx = Common.Approach(vx, -vxMax, tempAccel);
        }

        //Right
        if (!kLeft && kRight && !sticking)
        {
            // Apply acceleration left
            vx = Common.Approach(vx, vxMax, tempAccel);
        }

        // Friction
        if (!kRight && !kLeft)
            vx -= vx * tempFric * Time.deltaTime;

        // Wall jump
        if (kJumpDown && cLeft && !onGround)
        {
            if (kLeft)
            {
                vy = jumpHeight * 1.1f;
                vx = jumpHeight * .75f;
            }
            else
            {
                vy = jumpHeight * 1f;
                vx = vxMax;
            }
        }
        if (kJumpDown && cRight && !onGround)
        {
            if (kRight)
            {
                vy = jumpHeight * 1.1f;
                vx = -jumpHeight * .75f;
            }
            else
            {
                vy = jumpHeight * 1f;
                vx = -vxMax;
            }
        }

        // Jump 
        if (kJumpDown)
        {
            if (onGround)
                vy = jumpHeight;
            // Variable jumping
        }
        else if (kJumpRelease)
        {
            if (vy > 0)
                vy *= 0.25f;
        }

        //Refresh velocity
        rigidBody2d.velocity = new Vector2(vx, vy);
    }

    void LateUpdate()
    {
        if (clingAlarm > 0 && sticking == true)
            clingAlarm -= 1;
        else
            clingAlarm = 0;
    }

    void FixedUpdate()
    {


        usingRope = rope.ropeIsActive;
        if (usingRope == true)
        {
            Vector2 vecPlayer2Anchor = rope.anchorPoint - gameObject.GetComponent<Rigidbody2D>().position;
            Vector2 radialDirection = vecPlayer2Anchor.normalized;
            float radialSpeed = Vector2.Dot(GetComponent<Rigidbody2D>().velocity, radialDirection);

            if (radialSpeed < 0f)
                GetComponent<Rigidbody2D>().velocity -= radialSpeed * radialDirection;

            Vector2 tangencialDirection = -1f * new Vector2(radialDirection.y, radialDirection.x);
            GetComponent<Rigidbody2D>().velocity += tangencialDirection * Mathf.Sign(Vector2.Dot(GetComponent<Rigidbody2D>().velocity, tangencialDirection));
        }

    }
}
