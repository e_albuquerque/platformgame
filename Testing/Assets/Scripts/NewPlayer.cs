﻿using UnityEngine;
using System.Collections;

public class NewPlayer : MonoBehaviour {

	public float maxSpeedX, maxSpeedY;
	public float accel, groundAccel, airAccel;
	public float gravity;
	public float jumpSpeed;

	public bool cLeft;
	public bool cRight;
	public bool onGround;
	
	bool canStick;
	bool sticking;
	int clingAlarm = 0;
	
	public LayerMask whatIsSolid;
	public float solidCheckRadius = 0.01f;
	
	public Transform groundCheck,leftCheck,rightCheck;
	Collider2D groundCheckCol,leftCheckCol,rightCheckCol;
	
	bool kLeft, kRight, kUp, kDown, kJumpDown, kJumpRelease;
	
	Rigidbody2D rigidBody2d;
	
	float axisHorizontal, axisVertical;

	
	// Use this for initialization
	void Start () {

		canStick = true;
		sticking = false;
		
		onGround = false;
		cLeft = false;
		cRight = false;
		
		clingAlarm = 0;
		
		rigidBody2d = this.gameObject.GetComponent<Rigidbody2D>();
		
		groundCheckCol = groundCheck.GetComponent<BoxCollider2D>();
		leftCheckCol = leftCheck.GetComponent<BoxCollider2D>();
		rightCheckCol = rightCheck.GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
		axisHorizontal = Input.GetAxis ("Horizontal");
		axisVertical = Input.GetAxis ("Vertical");
		
		kLeft = (axisHorizontal < 0f) ? true : false;
		kRight = (axisHorizontal > 0f) ? true : false;
		kUp = (axisVertical > 0f) ? true : false;
		kDown = (axisVertical < 0f) ? true : false;
		
		kJumpDown = Input.GetButtonDown("Fire1");
		kJumpRelease = Input.GetButtonUp("Fire1");

		onGround = Physics2D.IsTouchingLayers( groundCheckCol,whatIsSolid);
		cLeft = Physics2D.IsTouchingLayers( leftCheckCol,whatIsSolid);
		cRight = Physics2D.IsTouchingLayers( rightCheckCol,whatIsSolid);

		if (onGround)
		{
			accel = groundAccel;
		}
		else
		{
			accel = airAccel;
		}

		float targetSpeedY;
		float currentSpeedY;
		currentSpeedY = rigidBody2d.velocity.y;
		targetSpeedY = -maxSpeedY;

		if (!onGround) {
			currentSpeedY = Common.ApproachUniform(- maxSpeedY, currentSpeedY,gravity);
			//currentSpeedY += gravity * Mathf.Sign (targetSpeedY - currentSpeedY);
			//if ( Mathf.Abs (currentSpeedY) > maxSpeedY)
			//	currentSpeedY = targetSpeedY;
			//currentSpeedY = Common.ApproachWeighted(-maxSpeedY,rigidBody2d.velocity.y,gravity);
		}

		float targetSpeedX;
		float currentSpeedX;
		currentSpeedX = rigidBody2d.velocity.x;
		targetSpeedX = maxSpeedX * axisHorizontal;

		//currentSpeedX += accel * Mathf.Sign (targetSpeedX - currentSpeedX);
		//if ( Mathf.Abs (currentSpeedX) > maxSpeedX)
		//	currentSpeedX = targetSpeedX;

		currentSpeedX = Common.ApproachWeighted( targetSpeedX,currentSpeedX, accel) ;

		//currentSpeedX = accel * targetSpeedX + (1 - accel) * currentSpeedX;
		//if (Mathf.Abs (currentSpeedX) < 0.01f)
		//	currentSpeedX = 0;

		// Jump 
		if (kJumpDown) { 
			if (onGround)
				currentSpeedY += jumpSpeed;
			// Variable jumping
		} else if (kJumpRelease) { 
			if (currentSpeedY > 0)
				currentSpeedY *= 0.25f;
		}

		rigidBody2d.velocity = new Vector2 (currentSpeedX, currentSpeedY);
	}
		
}

